module.exports = function (grunt) {
    var pkg = grunt.file.readJSON('./package.json');
    pkg.webRoot = '';
    pkg.vendor_path = 'vendor/';
    pkg.modules = 'node_modues/';
    pkg.name = 'main';

    var vendor = {
        jquery: {
            description: 'jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has changed the way that millions of people write JavaScript.',
            path: '<%= pkg.vendor_path %>jquery/',
            js: ['jquery-1.12.0.min.js']
        },
        jqueryValidation: {
            description: 'jQuery Validation',
            path: '<%= pkg.vendor_path %>jqueryValidation/',
            js: ['jquery.validate.js']
        },
        es6:{
            description: 'Add Es6 functional',
            path: '<%= pkg.vendor_path %>es6/',
            js: ['es6-promise.js']
        },
        html5shiv: {
            description: 'The HTML5 Shiv enables use of HTML5 sectioning elements in legacy Internet Explorer and provides basic HTML5 styling for Internet Explorer 6-9, Safari 4.x (and iPhone 3.x), and Firefox 3.x.',
            path: '<%= pkg.vendor_path %>html5shiv/',
            js: ['html5shiv.min.js']
        },
        fontawesome: {
            description: 'Font awesome',
            path: '<%= pkg.vendor_path %>fontawesome/',
            css: ['font-awesome.min.css']
        },
        bootstrapFull: {
            description: 'bootstrap grid system',
            path: '<%= pkg.vendor_path %>bootstrap/',
            css: ['bootstrap.css', 'bootstrap-theme.css'],
            js: ['bootstrap.js']
        },
        bootstrapGrid: {
            description: 'bootstrap grid system',
            path: '<%= pkg.vendor_path %>bootstrap/',
            css: ['bootstrap.css']
        },
        datepicker: {
            description: 'datepicker',
            path: '<%= pkg.vendor_path %>datepicker/',
            css: ['bootstrap-datepicker.css'],
            js: ['bootstrap-datepicker.js']
        }
    };

    var commonVendors = [
        vendor.jquery,
        vendor.jqueryValidation,
        vendor.es6,
        vendor.bootstrapGrid
    ];

    var ie8Vendors = [vendor.html5shiv];

    var source = {
        js: getVendorPath(commonVendors, 'js')
            .concat(['<%= pkg.webRoot %>js/src/main.js']),
        js_ie8: getVendorPath(ie8Vendors, 'js'),
        css: getVendorPath(commonVendors, 'css')
            .concat(['<%= pkg.webRoot %>css/main.css'])
    };

    var config = {
        pkg: pkg,

        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: ['pkg'],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json', 'composer.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false
            }
        },
        // Concatenation of files
        concat: {
            js: {
                src: source.js,
                dest: '<%= pkg.webRoot %>js/<%= pkg.name %>.js'
            },
            js_ie8: {
                src: source.js_ie8,
                dest: '<%= pkg.webRoot %>js/ie8.js'
            },
            css: {
                src: source.css,
                dest: '<%= pkg.webRoot %>css/<%= pkg.name %>.css'
            }
        },
        // Minification of files
        uglify: {

            js: {
                files: {
                    '<%= pkg.webRoot %>js/<%= pkg.name %>.min.js': ['<%= pkg.webRoot %>js/<%= pkg.name %>.js']
                }
            },
            js_ie8: {
                files: {
                    '<%= pkg.webRoot %>js/ie8.min.js': ['<%= pkg.webRoot %>js/ie8.js']
                }
            }
        },
        clean: {
            css: ['<%= pkg.webRoot %>css/*.css'],
            js: ['<%= pkg.webRoot %>js/*.js'],
            build: ['build/**']
        },
        //Compress for build archive
        compress: {
            dist: {
                options: {
                    mode: 'tgz',
                    archive: 'build/build.tar.gz'
                },
                files: [
                    {
                        src: [
                            'app/**', '!app/cache/**', '!app/logs/**',
                            'src/**',
                            'vendor/**', '!vendor/bundle/**',
                            '<%= pkg.webRoot %>**', '!<%= pkg.webRoot %>vendor/**', '!<%= pkg.webRoot %>js/src/**', '!<%= pkg.webRoot %>css/src/**', '!<%= pkg.webRoot %>upload/**'
                        ],
                        dest: '/'
                    }
                ]
            }
        },
        // Watch files
        watch: {
            js: {
                files: source.js,
                tasks: ['js']
            },
            css: {
                files: source.css,
                tasks: ['css']
            },
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['clean:css', 'clean:js', 'svg', 'js', 'css']
            },
            svg: {
                files: ['<%= pkg.webRoot %>images/svg/*.svg', '<%= pkg.webRoot %>images/svg/**/*.svg'],
                tasks: ['svg']
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true,
                limit: 20
            },
            dev: [
                'watch:css',
                'watch:js',
                'watch:svg',
                'watch:gruntfile',
                'compass:watch'
            ]
        },
        // Styles
        //
        // Compass + sass
        compass: {
            options: {
                sassDir: '<%= pkg.webRoot %>css/src/',
                cssDir: '<%= pkg.webRoot %>css/',
                imagesDir: '<%= pkg.webRoot %>images/',
                httpImagesPath: '../images/',
                fontsDir: '<%= pkg.webRoot %>fonts/',
                outputStyle: 'expanded'
            },
            compile: {
            },
            watch: {
                options: {
                    watch:true
                }
            }
        },
        // Autoprefixer postprocessor
        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ff 24', 'ie 8', 'ie 7']
            },
            all: {
                src: [
                    '<%= pkg.webRoot %>css/<%= pkg.name %>.css'
                ]
            }
        },
        // CSS Optimizer - better then just minification
        csso: {
            
            all: {
                files: {
                    '<%= pkg.webRoot %>css/<%= pkg.name %>.min.css': ['<%= pkg.webRoot %>css/<%= pkg.name %>.css']
                }
            }
        },
        // Make png fallback from svg
        svg2png: {
            all: {
                files: [{
                    cwd: '<%= pkg.webRoot %>images/svg/',
                    src: ['*.svg', '**/*.svg'],
                    dest: '<%= pkg.webRoot %>images/svg2png/'
                }]
            }
        }
    };

    //grunt.util._.merge(config, require('./grunt/lint')(grunt));
    //grunt.util._.merge(config, require('./grunt/test')(grunt));
    //grunt.util._.merge(config, require('./grunt/deploy')(grunt));

    grunt.initConfig(config);

    require('matchdep')
        .filterDev('grunt-*')
        .forEach(grunt.loadNpmTasks);

    grunt.registerTask('dev', ['concurrent:dev']);

    // Styles: concat css + vendor -> autoprefixer -> csso -> remove compass output
    grunt.registerTask('css', ['concat:css', 'autoprefixer:all', 'csso:all']);

    // Scripts: concat js + vendor -> uglify
    grunt.registerTask('js', ['concat:js', 'uglify:js', 'concat:js_ie8', 'uglify:js_ie8']);

    // Images: svg2png -> optim all images
    grunt.registerTask('svg', ['svg2png:all']);

    grunt.registerTask('compile', [
        'clean:css',
        'clean:js',
        'svg',
        'compass:compile',
        'css',
        'js'
    ]);

    grunt.registerTask('verify', ['composer:install:no-interaction', 'lint:jenkins']);

    grunt.registerTask('assemble', [
        'composer:install:no-interaction:prefer-dist:optimize-autoloader:no-dev',
        'compile',
        'compress'
    ]);

    grunt.registerTask('build', ['clean:build', 'verify', 'assemble']);

    grunt.registerTask('default', ['compile']);

    // Helpers for work with vendors path
    function getVendorPath(vendors, ext) {
        var files = [];
        for (var paths in vendors) {
            if (vendors[paths][ext]) {
                for (var i = 0; i < vendors[paths][ext].length; i++) {
                    var path = vendors[paths].path + vendors[paths][ext][i];
                    files.push(path);
                }
            }
        }
        return files;
    }
};
