(function($){
    jQuery.fn.calendar24 = function(){

        var list_month = ['Января','Февраля','Марта','Апреля','Мая','Июня',
            'Июля','Августа','Сентября','Октября','Ноября','Декабря'];


        var getDate = function(year, mon){
            var m = (year && mon >= 0) ? new Date(year, mon, 1): new Date(),

                feb = (m.getFullYear() % 4 == 0)?29:28,

                list_daysInMonth = [31,feb,31,30,31,30,31,31,30,31,30,31],

                first_day  = new Date(m.getFullYear(), m.getMonth(),1);

            return {
                day         : m.getDate(),
                month       : m.getMonth(),
                monthName   : list_month[m.getMonth()],
                daysInMonth : list_daysInMonth[m.getMonth()],
                year        : m.getFullYear(),
                firstWeekDay: first_day.getDay()
            };
        };


        var getCalendar = function (year, mon, d) {
            var calendar = '',
                month = (year && mon >= 0)? getDate(year, mon): getDate(),
                d =(d)?d:getDate().day,
                pMonth = ((month.month - 1) > 0) ? month.month - 1: 11,
                prevMonth = getDate(month.year, pMonth),
                firstWeekDay = (month.firstWeekDay==0)?7:month.firstWeekDay,
                startDay = (firstWeekDay != 1)? prevMonth.daysInMonth - (firstWeekDay - 2) : 1,
                day = startDay, newDay = false, counter = 0,
                week_day = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

            week:
            while (!newDay){
                calendar += '<tr>';
                for (var j = 0; j < 7; j++){
                    if(week_day) {
                        calendar += '<td class="week-day">' + week_day[j] + '</td>';
                        if(j==6) week_day = false;
                    }
                    else if(startDay > 1){
                        calendar += '<td class="day-old">'+(day++)+'</td>';
                        if(day > prevMonth.daysInMonth){
                            day = 1;
                            startDay = null;
                        }
                    }
                    else{
                        if(day > month.daysInMonth && j == 0) {
                            calendar = calendar.substr(0, calendar.length - 4);
                            break week;
                        }
                        else if(day > month.daysInMonth || newDay){
                            if(!newDay) {
                                day = 1;
                                newDay = true;
                            }
                            calendar += '<td class="day-new">'+day+'</td>';
                        }
                        else
                            calendar += '<td class="day'+
                                ((parseInt(d)==day)?" selected":"")+
                                ((day == getDate().day && month.month == getDate().month && month.year == getDate().year)?
                                    " current-day": "")+
                                '">'+day+'</td>';
                        day++;
                    }
                }
                calendar +='</tr>';
                if(counter++ == 5 && newDay) break;
            }

            return '<table class="calendar24"><tbody>'+calendar+'</tbody></table>';
        };


        var selectDate = {
            month: function (mon) {
                var month = "";
                for(var i = 0; i < list_month.length; i++)
                    month += '<div class="option'+((mon && mon==i)?' selected':"")+
                        '" data-value="'+i+'">'+list_month[i]+'</div>';
                return month;
            },

            days: function (day, month, year) {
                var daysInMonth = (year && month >= 0)?getDate(year, month).daysInMonth:getDate().daysInMonth;
                var days = "";
                for(var i = 1; i <= daysInMonth; i++)
                    days += '<div class="option'+((day && day==i)?' selected':"")+'" data-value="'+i+'">'+i+'</div>';
                return days;
            },

            years: function (year) {
                var years="";
                for(var i = - 7; i <= 12; i++)
                    years += '<div class="option'+
                        ((year && year==getDate().year + i)?' selected':"")+
                        '" data-value="'+(getDate().year + i)+'">'+(getDate().year + i)+'</div>';
                return years;
            }
        };


        var makeCalendar = function () {
            var $inputDate = $(this),
                $wrap_date = '<div class="wrap-date"></div>',
                $getDate_block = '<div class="wrap-date__select">'+
                                    '<div class="wrap-date__select-inner wrap-date__select-inner-day">'+
                                        '<span class="wrap-date__select-inner_digit">'+getDate().day+'</span>'+
                                        '<div class="calendar__date day_name">'+selectDate.days(getDate().day)+'</div>'+
                                    '</div>'+

                                    '<div class="wrap-date__select-inner wrap-date__select-inner-month">'+
                                        '<span class="wrap-date__select-inner_digit">'+getDate().monthName+'</span>'+
                                        '<div class="calendar__date month_name">'+selectDate.month(getDate().month)+'</div>'+
                                    '</div>'+

                                    '<div class="wrap-date__select-inner wrap-date__select-inner-year">'+
                                        '<span class="wrap-date__select-inner_digit">'+getDate().year+'</span>'+
                                        '<div class="calendar__date years_name">'+selectDate.years(getDate().year)+'</div>'+
                                    '</div>'+
                                '</div>',
                $dateBlock = $inputDate.wrap($wrap_date).parent();


            $dateBlock.append($getDate_block).append('<div class="calendar_wrap">'+getCalendar()+'</div>');
            $inputDate.val(getDate().day+'/'+(getDate().month+1)+'/'+getDate().year);


            $dateBlock.on('click', '.wrap-date__select-inner', function () {
                if($(this).hasClass('wrap-date__select-inner-selected'))
                    $(this).removeClass('wrap-date__select-inner-selected');
                else {
                    $(this).parent().find('.wrap-date__select-inner-selected')
                        .removeClass('wrap-date__select-inner-selected');

                    var $selected_item = $(this).find('.option.selected');
                    if ($selected_item.length != 0) {
                        var $indent = ($selected_item.position().top > 100) ? 100 : 0;
                        $(this).find('.mCSB_container').css({top: -($selected_item.position().top - $indent)});
                    }
                    $(this).addClass('wrap-date__select-inner-selected');
                }
            });


            $('body').on('click', function(e){
                if ($(e.target).closest('.wrap-date__select-inner_digit').length) return;
                $('.wrap-date__select-inner-selected')
                    .removeClass('wrap-date__select-inner-selected');

                e.stopPropagation();
            });


            $dateBlock.on('click', '.calendar__date .option', function () {
                if($(this).hasClass('selected'))
                    return;

                $(this).parent().find('.option').removeClass('selected');
                $(this).addClass('selected');

                var $wrap_date = $(this).parents('.wrap-date'),
                    $select_inner = $(this).parents('.wrap-date__select-inner'),
                    $month = $wrap_date.find('.wrap-date__select-inner-month .option.selected').data('value'),
                    $year = $wrap_date.find('.wrap-date__select-inner-year .option.selected').data('value'),
                    $day = ($wrap_date.find('.wrap-date__select-inner-day .option.selected')
                        .data('value') > getDate($year, $month).daysInMonth)? false :$wrap_date
                        .find('.wrap-date__select-inner-day .option.selected').data('value'),

                    $daysWrap = $wrap_date.find('.calendar__date.day_name .option').parent();

                $select_inner.find('.wrap-date__select-inner_digit').text($(this).text());

                if($day==false){
                    $day = 1;
                    $wrap_date.find('.wrap-date__select-inner-day .wrap-date__select-inner_digit').text($day);
                }

                if($select_inner.hasClass('wrap-date__select-inner-month') || $select_inner
                        .hasClass('wrap-date__select-inner-year'))
                    $wrap_date.find('.calendar_wrap').html(getCalendar($year,$month,$day));

                else if($select_inner.hasClass('wrap-date__select-inner-day')){
                    $wrap_date.find('.calendar_wrap .day.selected').removeClass('selected');
                    $($wrap_date.find('.calendar_wrap .day')[$day-1]).addClass('selected');
                }

                if($select_inner.hasClass('wrap-date__select-inner-month'))
                    $daysWrap.html(selectDate.days($day, $month, $year));

                else if($select_inner.hasClass('wrap-date__select-inner-year') && $month == 1)
                    $daysWrap.html(selectDate.days($day, $month, $year));

                $inputDate.val($day+'/'+($month+1)+'/'+$year);
            });


            function replaceDataInSelect($select, day, month, year) {
                var $names = {day:day, month:month, year:year};

                for($name in $names){
                    var $digit = ($name=='month')?list_month[$names[$name]]:$names[$name];

                    if($name=='day')
                        $select.find('.wrap-date__select-inner-day .option')
                            .parent().html(selectDate.days(day, month, year));

                    $select.find('.wrap-date__select-inner-'+$name+' .wrap-date__select-inner_digit').text($digit);
                    $select.find('.wrap-date__select-inner-'+$name+' .option.selected').removeClass('selected');
                    $select.find('.wrap-date__select-inner-'+$name+' .option').each(function () {
                        if($(this).data('value') == $names[$name])
                            $(this).addClass('selected');
                    });
                }
            }


            $dateBlock.on('click', '.calendar24 .day, .calendar24 .day-old, .calendar24 .day-new', function () {

                if($(this).hasClass('selected')) return;

                var $wrap_date = $(this).parents('.wrap-date'),
                    $select = $wrap_date.find('.wrap-date__select'),
                    $day_select = $wrap_date.find('.wrap-date__select-inner-day'),
                    $day = parseInt($(this).text()),
                    $month = $wrap_date.find('.wrap-date__select-inner-month .option.selected').data('value'),
                    $year = $wrap_date.find('.wrap-date__select-inner-year .option.selected').data('value');

                if($year == undefined) return;

                if($(this).hasClass('day-old')){
                    var $pMonth = ($month==0)?11:$month-1,
                        $pYear = ($pMonth == 11) ? $year- 1:$year;
                    $wrap_date.find('.calendar_wrap').html(getCalendar($pYear,$pMonth,$day));
                    replaceDataInSelect($select, $day, $pMonth, $pYear);
                    $inputDate.val($day+'/'+($pMonth+1)+'/'+$pYear);
                }

                else if($(this).hasClass('day-new')){
                    var $nMonth = ($month==11)?0: $month + 1,
                        $nYear = ($nMonth == 0) ? $year + 1:$year;
                    $wrap_date.find('.calendar_wrap').html(getCalendar($nYear,$nMonth,$day));
                    replaceDataInSelect($select, $day, $nMonth, $nYear);
                    $inputDate.val($day+'/'+($nMonth+1)+'/'+$nYear);
                }

                else{
                    $wrap_date.find('.calendar24 .day.selected').removeClass('selected');
                    $(this).addClass('selected');
                    $day_select.find('.option.selected').removeClass('selected');
                    $($day_select.find('.option')[$day-1]).addClass('selected');
                    $day_select.find('.wrap-date__select-inner_digit').text($day);
                    $inputDate.val($day+'/'+($month+1)+'/'+$year);
                }
            });


            if($.fn.mCustomScrollbar){
                $dateBlock.find(".calendar__date").mCustomScrollbar({theme:"dark-3"});
            }
        };


        return this.each(makeCalendar);
    };
})(jQuery);