$(function () {
//=================================================================
// Response
//=================================================================

    var isResponsive = checkResponsive();

    var className = 'has-offcanvas',
        $body = $('body'),
        $pull = $('#pull', $body),

        append_closest = function (exclude, func) {
            $body.on('click', function(e){
                if ($(e.target).closest(exclude).length) return;
                func();
                e.stopPropagation();
            });
        };


    $pull.on('click', function () {
        $body.addClass(className).stop().animate( {
            left : 250
        } , 50);
    });


    append_closest('#off-canvas, #pull', function () {
        $body.stop().animate( {
            left : 0
        } , 50).removeClass(className);
    });


    //fix responsive design
    $('html').toggleClass('is-responsive', isResponsive);


    if (isResponsive) {
        $('head meta[name=viewport]').attr('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no');
    }
    else {
        $('head meta[name=viewport]').attr('content', 'width=device-width, maximum-scale=1');
    }


    function checkResponsive() {
        var cookieValue = 1;
        $.each(document.cookie.split(';'), function(index, value) {
            var cookie = value.split('=');
            if (cookie.length === 2 && $.trim(cookie[0]) === '_responsive') {
                cookieValue = parseInt($.trim(cookie[1]));
            }
        });
        return cookieValue === 1;
    }
});
